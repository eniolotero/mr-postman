///scr_set_sprite()
if (y_move > 0) {
    sprite_index = spr_player_down;
} else if (y_move < 0) {
    sprite_index = spr_player_up;
} else if (x_move != 0) {
    sprite_index = spr_player_side;
    image_xscale = x_move;
}

