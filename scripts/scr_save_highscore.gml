///scr_save_highscore(highscore)

var highscore = argument0;
var filename = "data.sav";

if (file_exists(filename)) {
    file_delete(filename);
}

var highscore_file = file_text_open_write(filename);

file_text_write_real(highscore_file, highscore);
file_text_close(highscore_file);

