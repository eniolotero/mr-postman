///scr_load_highscore()

var highscore = 0;
var filename = "data.sav";


if (file_exists(filename)) {
    var highscore_file = file_text_open_read(filename);
    highscore = file_text_read_real(highscore_file);
    file_text_close(highscore_file);
    return highscore;    
} else {
    scr_save_highscore(0);
    return 0;
}

