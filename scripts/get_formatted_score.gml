///get_formatted_score()

var score_str = string(score);

for (var i=1; i <= 7; i++) {
    if (string_length(score_str) <= i) {
        score_str = string_insert("0", score_str, 0);
    }
}

return score_str;
