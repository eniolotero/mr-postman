#define scr_take_card
///scr_take_card()
var mailbox = collision_point(x, y, obj_mailbox, false, false);

if (mailbox != noone) {
    if (mailbox.has_card == true) {
        var space = get_empty_space();
        
        if (space != -1) {
            cards[space] = mailbox.receptor;
            mailbox.receptor = -1;
            mailbox.has_card = false;
        }    
    }
}

#define get_empty_space
///get_empty_space()
var i = 1;

for (i=1; i <= 3; i++) {
    if (cards[i] == 0) {
        audio_play_sound(snd_take_card, 100, 0);
        return i
    }
}

return -1;