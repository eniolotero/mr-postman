///scr_setup_next_level(level)

var level = argument0;
level_position = 0;

//scr_increase_dificulty();

//first level is 1
level_length = global.batch_size;
var list_coc = level_length div 9;
var list_rest = level_length mod 9;

var list = ds_list_create();

for (var i=0; i < list_coc; i++) {
    ds_list_add(list, 1, 2, 3, 4, 5, 6, 7, 8, 9);
}

for (var i=0; i < list_rest; i++) {
    ds_list_add(list, 9 - irandom_range(1, 81) mod 9);
}

ds_list_shuffle(list);

emisor_list = list;
