///scr_draw_scores()

draw_set_font(fnt_arcade_16);
draw_set_color(make_color_rgb(24, 29, 177));

//DELIVERED LETTERS
draw_text(
    (background_x[0] * 2) + 24,
    (background_y[0] * 2) + 80,
    "DELIVERED  LETTERS"
);

//DELIVERED ON TIME
draw_text(
    (background_x[0] * 2) + 32,
    (background_y[0] * 2) + 104,
    "ON  TIME"
);
var on_time_width = string_width(string(global.delivered_on_time));
draw_text(
    (background_x[0] * 2) + (background_width[0] * 2) - 24 - on_time_width,
    (background_y[0] * 2) + 104,
    global.delivered_on_time
);
//DELIVERED ON TIME

//DELIVERED LATE
draw_text(
    (background_x[0] * 2) + 32,
    (background_y[0] * 2) + 128,
    "LATE"
);
var late_width = string_width(string(global.delivered_late));
draw_text(
    (background_x[0] * 2) + (background_width[0] * 2) - 24 - late_width,
    (background_y[0] * 2) + 128,
    global.delivered_late
);
//DELIVERED LATE

//TOTAL
var total_delivered = global.delivered_on_time + global.delivered_late;
draw_text(
    (background_x[0] * 2) + 32,
    (background_y[0] * 2) + 152,
    "TOTAL"
);
var total_width = string_width(string(total_delivered));
draw_text(
    (background_x[0] * 2) + (background_width[0] * 2) - 24 - total_width,
    (background_y[0] * 2) + 152,
    total_delivered
);
//TOTAL

//SCORE
draw_text(
    (background_x[0] * 2) + 24,
    (background_y[0] * 2) + 184,
    "SCORE"
);
var score_width = string_width(get_formatted_score());
draw_text(
    (background_x[0] * 2) + (background_width[0] * 2) - 24 - score_width,
    (background_y[0] * 2) + 208,
    get_formatted_score()
);
if (score > global.highscore) {
    if (show_highscore) {
        draw_set_font(fnt_arcade_20);
        var highscore_text = "NEW  HIGHSCORE";
        var highscore_text_width = string_width(highscore_text);
        draw_text(
            (background_x[0] * 2) + background_width[0] - (highscore_text_width div 2),
            (background_y[0] * 2) + 240,
            highscore_text
        )
        draw_set_font(fnt_arcade_16)
        highscore_loop++;
        if (highscore_loop >= highscore_loops_on) {
            highscore_loop = 0;
            show_highscore = false;
        }
    } else {
        highscore_loop++;
        if (highscore_loop >= highscore_loops_off) {
            highscore_loop = 0;
            show_highscore = true;
        }
    }
}
//SCORE

/*
//TIME PLAYED
draw_text(
    (background_x[0] * 2) + 24,
    (background_y[0] * 2) + 192,
    "TIME  PLAYED"
);
//TIME PLAYED
*/
