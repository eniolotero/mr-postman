///scr_read_controls()

if (keyboard_check_pressed(kb_action)) {
    action = 1;
}
if (keyboard_check(kb_up)) {
    y_move = -1;
} else if (keyboard_check(kb_down)) {
    if (y_move == -1) {
        y_move = 0;
    }
    y_move = 1;
}
if (keyboard_check(kb_left)) {
    x_move = -1;
}
if (keyboard_check(kb_right)) {
    if (x_move == -1) {
        x_move = 0;
    }
    x_move = 1;
}

if (x_move != 0) {
    y_move = 0;
}

if (x_move == 0 && y_move == 0) {
    image_speed = 0
} else {
    image_speed = animation_speed;
}

