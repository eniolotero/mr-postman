///player_has_cards(player)

var player = argument0;
var has_cards = false;

for (var i=1; i <= 3; i++) {
    if (player.cards[i] > 0) {
        has_cards = true;
        break;
    }
}

return has_cards;

