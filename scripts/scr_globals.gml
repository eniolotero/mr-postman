///scr_globals

enum pStatus {
    normal,
    gameEnd
};

//Load highscore
global.highscore = scr_load_highscore();

/*
Level dificulty variables
*/
global.min_letter_spawn_time = 30 * 3.5;
global.letter_spawn_time = 30 * 6;
global.spawn_time_decrease = 5;

global.min_letter_active_time = 30 * 3;
global.letter_active_time = 30 * 5;
global.active_time_decrease = 4;

global.min_letter_lose_score_timer = 30 * 2;
global.letter_lose_score_timer = 30 * 6;
global.lose_score_timer_decrease = 8;

//Cards spawned before stop
global.batch_size = 5;
global.batch_size_inc_factor = 1.1;
/*
End of level dificulty variables
*/

global.letter_score_hi = 10;
global.letter_score_low = 5;
global.max_letters_missed = 3;

global.letters_missed = 0;
global.delivered_on_time = 0;
global.delivered_late = 0;

