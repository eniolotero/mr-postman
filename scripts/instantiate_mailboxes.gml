var x_1, x_2, x_3, y_1, y_2, y_3;

x_1 = 64;
x_2 = 160;
x_3 = 256;
y_1 = 32;
y_2 = 96;
y_3 = 160;

mailboxes[1] = instance_create(x_1, y_1, obj_mailbox);
mailboxes[2] = instance_create(x_2, y_1, obj_mailbox);
mailboxes[3] = instance_create(x_3, y_1, obj_mailbox);
mailboxes[4] = instance_create(x_1, y_2, obj_mailbox);
mailboxes[5] = instance_create(x_2, y_2, obj_mailbox);
mailboxes[6] = instance_create(x_3, y_2, obj_mailbox);
mailboxes[7] = instance_create(x_1, y_3, obj_mailbox);
mailboxes[8] = instance_create(x_2, y_3, obj_mailbox);
mailboxes[9] = instance_create(x_3, y_3, obj_mailbox);

mailboxes[1].mailbox_id = 1;
mailboxes[2].mailbox_id = 2;
mailboxes[3].mailbox_id = 3;
mailboxes[4].mailbox_id = 4;
mailboxes[5].mailbox_id = 5;
mailboxes[6].mailbox_id = 6;
mailboxes[7].mailbox_id = 7;
mailboxes[8].mailbox_id = 8;
mailboxes[9].mailbox_id = 9;

