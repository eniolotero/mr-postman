///scr_increase_dificulty()

if (global.letter_spawn_time > global.min_letter_spawn_time) {
    //Time before spawning new letter
    global.letter_spawn_time -= global.spawn_time_decrease;
    global.letter_spawn_time = max(
        global.letter_spawn_time,
        global.min_letter_spawn_time
    );
    
    //Time before the letter disappear
    global.letter_active_time -= global.active_time_decrease
    global.letter_active_time = max(
        global.letter_active_time,
        global.min_letter_active_time
    );
    
    //Time before letter gets it's score reduced
    global.letter_lose_score_timer -= global.lose_score_timer_decrease;
    global.letter_lose_score_timer = max(
        global.letter_lose_score_timer,
        global.min_letter_lose_score_timer
    );

    //Cards to spawn in next level
    global.batch_size *= global.batch_size_inc_factor;
}

