///letter_delivered(score)

var card_score = argument0;

if (card_score == global.letter_score_hi) {
    global.delivered_on_time++;
} else {
    global.delivered_late++;
}
