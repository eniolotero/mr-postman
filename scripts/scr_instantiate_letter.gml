#define scr_instantiate_letter
///scr_instantiate_letter(list_position)

var list_position = argument0;
var emisor_id = ds_list_find_value(emisor_list, list_position);
var receptor_id = 0;

while (receptor_id == 0) {
    receptor_id = 9 - irandom_range(1, 81) mod 9;
    if (emisor_id == receptor_id) {
        receptor_id = 0;
    }
}

mailbox_set_card(emisor_id, receptor_id);
audio_play_sound(snd_new_letter, 100, 0);


#define in_array
///in_array(mailbox_id, mailbox_array)

var mailbox_id = argument0;
var mailbox_array = argument1;

for (var i=1; i <= 3; i++) {
    if (mailbox_array[i] == mailbox_id) {
        return true;
    }
}

return false;

#define array_push
///array_push(item, array)

var item = argument0;
var array = argument1;


for (var i = 3; i >= 1; i++) {
    if (i > 1) {
        array[i] = array[i-1];
    } else {
        array[i] = item;
    }
}