///mailbox_set_card(emisor, receptor)
var emisor = argument0;
var receptor = argument1;

mailboxes[emisor].has_card = true;
mailboxes[emisor].receptor = receptor;
mailboxes[emisor].alarm[0] = global.letter_active_time;

