#define scr_mailbox_interact
///scr_mailbox_interact()
var mailbox = collision_point(x, y, obj_mailbox, false, false);

if (mailbox != noone) {
    var mail_delivered = deliver_mail(mailbox.mailbox_id);
    
    if (!mail_delivered) {
        if (mailbox.has_card == true) {
            var bagSpace = get_empty_space();
            scr_take_card(mailbox);
        }
    }
}

#define deliver_mail
///deliver_mail(mailbox_id)
var mailbox_id = argument0;
var i = 1;
var card_id = -1;

for (i=1; i <= 3; i++) {
    if (card_id == -1) {
        //leave the card
        if (cards[i] == mailbox_id) {
            //Deliver a card from it's value
            letter_delivered(card_score[i]);
            card_id = i;
            cards[i] = 0;
            //add score
            score += card_score[i];
            card_score[i] = 0;
            audio_play_sound(snd_deliver, 100, 0);
            return true;
        }
    }/* else {
        //reorder card array
        cards[i-1] = cards[i];
        cards[i] = 0;
    }
    */
}

return false;

#define scr_take_card
///scr_take_card(mailbox)
var mailbox = argument0;

if (mailbox.has_card == true) {
    var bagSpace = get_empty_space();
    
    if (bagSpace != -1) {
        cards[bagSpace] = mailbox.receptor;
        card_score[bagSpace] = global.letter_score_hi;
        alarm[bagSpace] = global.letter_lose_score_timer;
        mailbox.receptor = -1;
        mailbox.has_card = false;
    }    
}





#define get_empty_space
///get_empty_space()
var i = 1;

for (i=1; i <= 3; i++) {
    if (cards[i] == 0) {
        audio_play_sound(snd_take, 100, 0);
        return i;
    }
}

return -1;