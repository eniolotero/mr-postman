#define scr_leave_card
///scr_leave_card()
var mailbox = collision_point(x, y, obj_mailbox, false, false);

if (mailbox != noone) {
    var result = deliver_mail(mailbox.mailbox_id);
}

#define deliver_mail
///deliver_mail(mailbox_id)
var mailbox_id = argument0;
var i = 1;
var card_id = -1;

for (i=1; i <= 3; i++) {
    if (card_id == -1) {
        //leave the card
        if (cards[i] == mailbox_id) {
            card_id = i;
            cards[i] = 0;
            audio_play_sound(snd_leave_card, 100, 0);
            return true;
        }
    }/* else {
        //reorder card array
        cards[i-1] = cards[i];
        cards[i] = 0;
    }
    */
}

return false;
